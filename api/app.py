import logging
import falcon
from functools import partial

from api.config import parser, settings
from api.resources.routes import setup as setup_routes
from api.media import json
from api.util.vyper import setup_vyper
from api.util.logging import setup_logging
from api.util.error import error_handler
from api.util.middleware import AuthMiddleware, RequireHTTPS

from api.scheduler import setup_scheduler
from api.resources.users import setup_user

logger = logging.getLogger(__name__)


def configure(**overrides):
	logging.getLogger("pymongo").setLevel(level=logging.INFO)
	logging.getLogger("requests").setLevel(level=logging.INFO)
	logging.getLogger("urllib3").setLevel(level=logging.INFO)
	logging.getLogger("apscheduler").setLevel(level=logging.INFO)
	logging.getLogger('vyper').setLevel(level=logging.WARNING)
	logging.getLogger('werkzeug').setLevel(level=logging.INFO)

	setup_vyper(parser, overrides)
	setup_logging()



def create_app():
	# falcon.API instances are callable WSGI apps
	app = falcon.API(middleware=[AuthMiddleware()])
	#app = falcon.API()
	
	dump_kwargs = {"ensure_ascii": False, "sort_keys": False}
	kwargs = json.add_settings_to_kwargs({})
	dump_kwargs.update(kwargs)

	json_handler = falcon.media.JSONHandler(
		dumps=partial(json.dumps, **dump_kwargs),
		loads=partial(json.loads, **kwargs),
	)
	extra_handlers = {
		falcon.MEDIA_JSON: json_handler,
	}

	app.req_options.media_handlers.update(extra_handlers)
	app.resp_options.media_handlers.update(extra_handlers)
	app.add_error_handler(Exception, error_handler)

	setup_routes(app)

	return app



def start():
	logger.info("Starting {}".format(settings.get("APP_NAME")))
	logger.info("Environment: {}".format(settings.get("ENV_NAME")))

	setup_scheduler()
	setup_user()


