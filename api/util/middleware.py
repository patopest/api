import falcon
import jwt
import datetime
import re

from api.config import settings

class AuthMiddleware(object):
	def __init__(self):
		self.exempt_routes = []
		self.account_types = ['admin','user']

	def process_resource(self, req, resp, resource, params):
		settings = self._get_resource_settings(req, resource)

		if req.path in settings['exempt_routes']:
			return
		token = req.get_header('Authorization')


		if token is None:
			raise falcon.HTTPUnauthorized(description="Please provide authentification")

		token = self._decode_token(token)

		if not self._token_is_valid(token):
			raise falcon.HTTPUnauthorized(description="Invalid token")

		if token['type'] not in settings['types']:
			raise falcon.HTTPUnauthorized(description="Invalid priviledges")

		req.context['token'] = token
			
		return


	def _token_is_valid(self, decoded_token):
		#decoded = jwt.decode(token, settings.get('SECRET_KEY'), algorithm='HS256')
		if decoded_token['exp'] <= int(datetime.datetime.utcnow().timestamp()):
			return False
		else:
			return True

	def _decode_token(self, token):
		try:
			decoded = jwt.decode(token, settings.get('SECRET_KEY'), algorithm='HS256')
		except jwt.exceptions.ExpiredSignatureError:
			raise falcon.HTTPUnauthorized(description="Invalid token")
		return decoded

	def _get_resource_settings(self, req, resource):
		settings = getattr(resource, 'auth', {})
		settings['exempt_routes'] = []
		if 'disabled' in settings.keys():
			if settings['disabled'] == True:
				settings['exempt_routes'].append(req.uri_template)
		if 'types' not in settings.keys():
			settings['types'] = self.account_types
		
		return settings



class RequireHTTPS(object):
    """Middleware to verify that each request is performed via HTTPS.
    While the web server is primarily responsibile for enforcing the
    HTTPS protocol, misconfiguration is still a leading cause of
    security vulnerabilities, and so it can be helpful to perform
    certain additional checks, such as this one, within the application
    layer itself.
    At least one of the following sources must indicate the use of
    HTTPS:
        * The requested URI
        * The X-Forwarded-Proto header
        * The Forwarded header
    Otherwise, an instance of falcon.HTTPBadRequest is raised.
    """
    def __init__(self):
    	self._FORWARDED_PROTO_RE = re.compile('proto=([A-Za-z]+)')

    def process_request(self, req, resp):
        # NOTE: replaced deprecated req.protocol
        #       with req.scheme since it removed in Falcon v2.0.
        if req.scheme.lower() == 'https':
            return

        xfp = req.get_header('X-FORWARDED-PROTO')
        if xfp and xfp.lower() == 'https':
            return

        forwarded = req.get_header('FORWARDED')
        if forwarded:
            # NOTE(kgriffs): The first hop must indicate HTTPS,
            #   otherwise the chain is already insecure.
            first, __, __ = forwarded.partition(',')

            match = self._FORWARDED_PROTO_RE.search(first)
            if match and match.group(1).lower() == 'https':
                return

        raise falcon.HTTPBadRequest(
            title='HTTPS Required',
            description=(
                'All requests must be performed via the HTTPS protocol. '
                'Please switch to HTTPS and try again.'
            )
        )	