from pydantic import validator
from datetime import datetime

from api.data.model import Model
from api.config import settings

from api.scheduler import get_scheduler as scheduler

class TaskModel(Model):
	user: str = ""
	email: str = ""

	dept: str = ""
	code: str = ""
	crn: str = ""
	term: str = ""

	rate: int = 0
	last_run: datetime = None

	def dump(self) -> dict:
		model = super().to_dict()
		output = {
			"id": model['_id'],
			"course": {
				"dept": model['dept'],
				"code": model['code'],
				"crn": model['crn'],
				"term": model['term']
			},
			"user": model['user'],
			"email": model['email'],
			"active": scheduler().get_task(model['_id']),
			"rate": model['rate'],
			"last_run_at": model['last_run'],
			"metadata": {
				"created_at": model['created_at'],
				"updated_at": model['updated_at'],
				"deleted_at": model['deleted_at']
			}
		}
		return output





