import falcon
import jwt
from hashlib import sha256

from api.media.validators.jsonschema import load_schema, validate
from api.resources import Resource
from api.data import retrieve_model
from api.util.error import HTTPError
from ..mappers import TaskMapper
from ..models import TaskModel
from api.resources.users.mappers import UserMapper


def schema():
    task = load_schema("../schemas/task.json")
    create = load_schema("../schemas/create.json")
    task['properties']['course'].update(create)
    return task


def validate_user(req, resp, resource, params):
    token = req.context.get('token')
    _db = getattr(resource, '_db')
    if token['type'] != 'admin':
        if params['user_id'] != token['id']:
            raise HTTPError(falcon.HTTP_FORBIDDEN, "")

    user = UserMapper(_db).find(user_id=params['user_id'])
    if user:
        return
    return HTTPError(falcon.HTTP_NOT_FOUND, "User not found")


@falcon.before(validate_user)
class UserTasks(Resource):
    def __init__(self, db):
        super().__init__(db)
        self.auth = {"types": ["admin", "user"]}

    def on_get(self, req, resp, user_id):
        mapper = TaskMapper(self._db)
        tasks = mapper.find_by_user_id(user_id)
        if tasks:
            tasks = [task.dump() for task in tasks]
        else:
            tasks = []

        resp.media = tasks

    @validate(schema())
    def on_post(self, req, resp, user_id):
        mapper = TaskMapper(self._db)
        user = UserMapper(self._db).find(user_id=user_id)
        if user:
            req.media['user'] = str(user.id)
        else:
            raise HTTPError(falcon.HTTP_NOT_FOUND, "User not found")
        if user and req.media.get('email') is None:
            req.media['email'] = user.email
        

        req.media.update(req.media['course'])
        req.media.pop('course')
        task = TaskModel(**req.media)

        task = mapper.insert(task)

        resp.status = falcon.HTTP_CREATED
        resp.media = task.dump()

    def on_get_single(self, req, resp, user_id, task_id):
        task = retrieve_model(task_id, TaskMapper(self._db))

        resp.media = task.dump()

    @validate(schema())
    def on_put_single(self, req, resp, user_id, task_id):
        mapper = TaskMapper(self._db)
        task = retrieve_model(task_id, mapper)

        req.media.update(req.media['course'])
        req.media.pop('course')
        task = task.update(req.media)
        mapper.update(task)

        resp.media = task.dump()

    def on_delete_single(self, req, resp, user_id, task_id):
        mapper = TaskMapper(self._db)
        task = retrieve_model(task_id, mapper)

        task.delete()
        mapper.delete(task)

        resp.status = falcon.HTTP_NO_CONTENT
