import falcon

from api.data import retrieve_model
from api.media.validators.jsonschema import load_schema, validate
from api.resources import Resource
from ..mappers import TaskMapper


def schema():
    return load_schema("../schemas/task.json")


class Task(Resource):
    def __init__(self, db):
        super().__init__(db)
        self.auth = {"types": ["admin"]}
    
    def on_get(self, req, resp, task_id):
        task = retrieve_model(task_id, TaskMapper(self._db))

        resp.media = task.dump()

    @validate(schema())
    def on_put(self, req, resp, task_id):
        mapper = TaskMapper(self._db)
        task = retrieve_model(task_id, mapper)

        req.media.update(req.media['course'])
        req.media.pop('course')
        task = task.update(req.media)
        mapper.update(task)

        resp.media = task.dump()

    def on_delete(self, req, resp, task_id):
        mapper = TaskMapper(self._db)
        task = retrieve_model(task_id, mapper)

        task.delete()
        mapper.delete(task)

        resp.status = falcon.HTTP_NO_CONTENT


class TaskSchema:
    def __init__(self):
        self.auth = {"disabled": True}

    def on_get(self, req, resp):
        resp.media = schema()
