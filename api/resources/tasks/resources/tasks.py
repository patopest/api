import falcon

from api.media.validators.jsonschema import load_schema, validate
from api.resources import Resource
from api.util.error import HTTPError
from ..mappers import TaskMapper
from ..models import TaskModel
from api.resources.users.mappers import UserMapper


def schema():
    task = load_schema("../schemas/task.json")
    create = load_schema("../schemas/create.json")
    task['properties']['course'].update(create)
    return task


class Tasks(Resource):
    def __init__(self, db):
        super().__init__(db)
        self.auth = {"types": ["admin"]}

    def on_get(self, req, resp):
        mapper = TaskMapper(self._db)
        tasks = [task.dump() for task in mapper.find()]

        resp.media = tasks

    @validate(schema())
    def on_post(self, req, resp):
        mapper = TaskMapper(self._db)
        user = UserMapper(self._db).find(user_id=req.context.get('token')['id'])
        if user:
            req.media['user'] = str(user.id)
        if user and req.media.get('email') is None:
            req.media['email'] = user.email
        elif req.media.get('email') is None:
            raise HTTPError(falcon.HTTP_CONFLICT, "Please provide a user or email address.")

        req.media.update(req.media['course'])
        req.media.pop('course')
        task = TaskModel(**req.media)

        task = mapper.insert(task)

        resp.status = falcon.HTTP_CREATED
        resp.media = task.dump()
