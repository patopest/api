import falcon

from api.data.db import setup

from .task import Task, TaskSchema
from .tasks import Tasks
from .user_tasks import UserTasks


def get_routes(app: falcon.API):
    db = setup()['minerva']

    app.add_route("/tasks", Tasks(db))
    app.add_route("/tasks/{task_id}", Task(db))
    app.add_route("/task.schema.json", TaskSchema())

    app.add_route("/users/{user_id}/tasks", UserTasks(db))
    app.add_route("/users/{user_id}/tasks/{task_id}", UserTasks(db), suffix="single")
