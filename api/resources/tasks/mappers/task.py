from typing import List, Union

from xid import XID

from api.data.mapper import Mapper
from ..models import TaskModel

from api.scheduler import get_scheduler as scheduler


class TaskMapper(Mapper):
    def __init__(self, db):
        super().__init__(db)

    def insert(self, task: TaskModel) -> TaskModel:
        self._db.tasks.insert_one(task.to_dict())
        scheduler().create_task(str(task.id))
        return self.find(task.id)

    def find(
        self, task_id: Union[XID, None] = None
    ) -> Union[TaskModel, List[TaskModel], None]:
        if task_id:
            task = self._db.tasks.find_one({"_id": str(task_id)})
            if task:
                return TaskModel(**task)
            else:
                return []

        if self._db.tasks.count_documents({}) < 1:
            return []

        tasks = []
        for task in self._db.tasks.find():
            o = TaskModel(**task)
            if getattr(o, "deleted_at") and o.deleted_at is not None:
                continue
            else:
                tasks.append(o)

        return tasks

    def find_by_user_id(self, user_id: XID) -> List[TaskModel]:
        tasks = []
        for task in self._db.tasks.find({"user": str(user_id)}):
            o = TaskModel(**task)
            if getattr(o, "deleted_at") and o.deleted_at is not None:
                continue
            else:
                tasks.append(o)
        return tasks

    def update(self, task: TaskModel) -> TaskModel:
        result = scheduler().edit_task(str(task.id))
        task = self._db.tasks.update({"_id": str(task.id)}, task.to_dict(), upsert=True)
        return task

    def delete(self, task: TaskModel):
        self._db.tasks.update({"_id": str(task.id)}, task.to_dict())
        scheduler().delete_task(str(task.id))


    
