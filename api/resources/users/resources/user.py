import falcon
import copy
from hashlib import pbkdf2_hmac

from api.data import retrieve_model
from api.media.validators.jsonschema import load_schema, validate
from api.resources import Resource
from api.util.error import HTTPError
from ..mappers import UserMapper
from ..mappers import AuthMapper

from api.scheduler import test_notification


def schema():
    return load_schema("../schemas/user.json")


def validate_user(req, resp, resource, params):
    token = req.context.get('token')
    _db = getattr(resource, '_db')
    if token['type'] != 'admin':
        if params['user_id'] != token['id']:
            raise HTTPError(falcon.HTTP_FORBIDDEN, "")

    user = UserMapper(_db).find(user_id=params['user_id'])
    if user:
        return
    return HTTPError(falcon.HTTP_NOT_FOUND, "User not found")


@falcon.before(validate_user)
class User(Resource):
    def __init__(self, db):
        super().__init__(db)
        self.auth = {"types": ["admin", "user"]}

    def on_get(self, req, resp, user_id):
        user = retrieve_model(user_id, UserMapper(self._db))

        resp.media = user.dump()

    #@validate(schema())
    def on_put(self, req, resp, user_id):
        if req.context.get('token').get('type') != "admin":
            req.media.pop('type')

        auth_mapper = AuthMapper(self._db)
        user_creds = auth_mapper.find_by_email(email=req.media['email'])
        mapper = UserMapper(self._db)
        user = mapper.find_by_email(email=req.media['email'])

        if user_creds is None or user is None:
            raise HTTPError(falcon.HTTP_NOT_FOUND, "No such user")
        password = pbkdf2_hmac('sha256', req.media['password'].encode(), settings.get('SECRET_KEY').encode(), 200000).hex()
        if password != user_creds.password:
            raise HTTPError(falcon.HTTP_BAD_REQUEST, "Wrong login")

        new_password = pbkdf2_hmac('sha256', req.media['new_password'].encode(), settings.get('SECRET_KEY').encode(), 200000).hex()
        data = copy.deepcopy(req.media)
        data.pop('new_password')
        data['password'] = new_password

        user_creds = user_creds.update(data)
        auth_mapper.update(user_creds)

        user = user.update(data)
        mapper.update(user)

        resp.media = user.dump()

    def on_delete(self, req, resp, user_id):
        mapper = UserMapper(self._db)
        user = retrieve_model(user_id, mapper)

        user.delete()
        mapper.delete(user)

        auth_mapper = AuthMapper(self._db)
        try:
            user = retrieve_model(user_id, auth_mapper)
            auth_mapper.delete(user)
        except HTTPError:
            pass
            
        resp.status = falcon.HTTP_NO_CONTENT

@falcon.before(validate_user)
class UserNotifier(Resource):
    def __init__(self, db):
        super().__init__(db)
        self.auth = {"types": ["admin", "user"]}

    def on_post(self, req, resp, user_id):
        if 'email' in req.media:
            test_notification(req.media['email'])
            resp.status = falcon.HTTP_201
        else:
            raise HTTPError(falcon.HTTP_BAD_REQUEST, "No email provided")

class UserSchema:
    def __init__(self):
        self.auth = {"disabled": True}

    def on_get(self, req, resp):
        resp.media = schema()

