import falcon

from api.data.db import setup

from .auth import Login, Register, AuthSchema
from .user import User, UserNotifier, UserSchema
from .users import Users


def get_routes(app: falcon.API):
    db = setup()['minerva']

    app.add_route("/login", Login(db))
    app.add_route("/register", Register(db))
    app.add_route("/auth.schema.json", AuthSchema())

    app.add_route("/users", Users(db))
    app.add_route("/users/{user_id}", User(db))
    app.add_route("/users/{user_id}/notifier", UserNotifier(db))
    app.add_route("/user.schema.json", UserSchema())
