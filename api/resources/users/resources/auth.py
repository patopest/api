import falcon
import logging
import datetime
import copy
import jwt
from hashlib import pbkdf2_hmac

from api.config import settings
from api.media.validators.jsonschema import load_schema, validate
from api.resources import Resource
from api.util.error import HTTPError
from ..mappers import UserMapper, AuthMapper
from ..models import UserModel, AuthModel

def schema():
	user = load_schema("../schemas/auth.json")
	return user

class Login(Resource):
	def __init__(self, db):
		super().__init__(db)
		self.auth = {"disabled": True}

	@validate(schema())
	def on_post(self, req, resp):
		login_mapper = AuthMapper(self._db)
		user_creds = login_mapper.find_by_email(email=req.media['email'])
		mapper = UserMapper(self._db)
		user = mapper.find_by_email(email=req.media['email'])

		if user_creds is None or user is None:
			raise HTTPError(falcon.HTTP_NOT_FOUND, "No such user")
		password = pbkdf2_hmac('sha256', req.media['password'].encode(), settings.get('SECRET_KEY').encode(), 200000).hex()

		if password != user_creds.password:
			raise HTTPError(falcon.HTTP_BAD_REQUEST, "Wrong login")

		token = create_token(str(user.id), user.user_type).decode()
		response = {
			"id": str(user.id),
			"email": user.email,
			"token": token,
			"valid_for": settings.get("TOKEN_VALIDITY")*60*60
		}

		resp.status = falcon.HTTP_ACCEPTED
		resp.media = response


class Register(Resource):
	def __init__(self, db):
		super().__init__(db)
		self.auth = {"disabled": True}

	@validate(schema())
	def on_post(self, req, resp):
		auth_mapper = AuthMapper(self._db)

		req.media['password'] = pbkdf2_hmac('sha256', req.media['password'].encode(), settings.get('SECRET_KEY').encode(), 200000).hex()
		user_creds = AuthModel(**req.media)

		user_mapper = UserMapper(self._db)
		data = copy.deepcopy(req.media)
		data.pop('password')
		data['id'] = str(user_creds.id)
		user = UserModel(**data)

		user_creds.user_id = str(user.id)

		try:
			user = user_mapper.insert(user)
		except ValueError:
			raise HTTPError(falcon.HTTP_CONFLICT, "Email address already in-use")

		user_creds = auth_mapper.insert(user_creds)

		token = create_token(str(user.id), user.user_type).decode()
		response = {
			"id": str(user.id),
			"email": user.user_type,
			"token": token,
			"valid_for": settings.get("TOKEN_VALIDITY")*60*60
		}

		resp.status = falcon.HTTP_CREATED
		resp.media = response


def create_token(account_id, account_type):
	payload = {
		'exp': int((datetime.datetime.utcnow() + datetime.timedelta(hours=settings.get("TOKEN_VALIDITY"), seconds=0)).timestamp()),
		'iat': int(datetime.datetime.utcnow().timestamp()),
		'id': account_id,
		'type': account_type
	}
	return jwt.encode(payload, settings.get('SECRET_KEY'), algorithm='HS256')


class AuthSchema:
	def on_get(self, req, resp):
		resp.media = schema()
