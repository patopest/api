import falcon

from api.media.validators.jsonschema import load_schema, validate
from api.resources import Resource
from api.util.error import HTTPError
from ..mappers import UserMapper
from ..models import UserModel


def schema():
    user = load_schema("../schemas/user.json")
    create = load_schema("../schemas/create.json")
    user = user.update(create)
    return user


class Users(Resource):
    def __init__(self, db):
        super().__init__(db)
        self.auth = {"types": ["admin"]}

    def on_get(self, req, resp):
        mapper = UserMapper(self._db)
        users = [user.dump() for user in mapper.find()]

        resp.media = users

    @validate(schema())
    def on_post(self, req, resp):
        mapper = UserMapper(self._db)
        user = UserModel(**req.media)

        try:
            user = mapper.insert(user)
        except ValueError:
            raise HTTPError(falcon.HTTP_CONFLICT, "Email address already in-use")

        resp.status = falcon.HTTP_CREATED
        resp.media = user.dump()
