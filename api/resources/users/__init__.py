import logging
from .resources import get_routes

logger = logger = logging.getLogger("setup_user")

def setup_user():
	from hashlib import pbkdf2_hmac

	from api.data.db import setup
	from api.config import settings

	from .models import UserModel, AuthModel
	from .mappers import UserMapper, AuthMapper

	db = setup()['minerva']

	base_user = {
		"email": settings.get('BASE_USER'),
		"user_type": "admin"
	}
	
	mapper = UserMapper(db)
	user = UserModel(**base_user)
	try:
		mapper.insert(user)
	except ValueError:
		logger.info("Base user with email: {} already found".format(settings.get('BASE_USER')))
		return

	base_user['user_id'] = str(user.id)
	base_user['password'] = pbkdf2_hmac('sha256', settings.get('BASE_PASSWORD').encode(), settings.get('SECRET_KEY').encode(), 200000).hex()
	mapper = AuthMapper(db)
	user = AuthModel(**base_user)
	mapper.insert(user)
	logger.info("Created base user with email: {}".format(settings.get('BASE_USER')))

