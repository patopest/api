from typing import List, Union

from xid import XID

from api.data.mapper import Mapper
from ..models import UserModel


class UserMapper(Mapper):
    def __init__(self, db):
        super().__init__(db)

    def insert(self, user: UserModel) -> UserModel:
        exists = self._find_by_email_or_id(user.email, user.id)
        if exists:
            raise ValueError

        self._db.users.insert_one(user.to_dict())

        return self.find(user.id)

    def find(
        self, user_id: Union[XID, str] = None
    ) -> Union[UserModel, List[UserModel], None]:
        if user_id:
            user = self._db.users.find_one({"_id": str(user_id)})
            if user:
                return UserModel(**user)

        if self._db.users.count_documents({}) < 1:
            return []

        users = []
        for user in self._db.users.find():
            o = UserModel(**user)
            if getattr(o, "deleted_at") and o.deleted_at is not None:
                continue
            else:
                users.append(o)

        return users

    def find_by_email(self, email: str) -> Union[UserModel, None]:
        user = self._db.users.find_one({"email": email})
        if user:
            return UserModel(**user)
        return None

    def update(self, user: UserModel) -> UserModel:
        self._db.users.update({"_id": str(user.id)}, user.to_dict(), upsert=True)
        return user

    def delete(self, user: UserModel):
        self._db.users.update({"_id": str(user.id)}, user.to_dict())

    def _find_by_email_or_id(self, email: str, user_id: XID) -> Union[UserModel, None]:
        user = self._db.users.find_one({"$or": [{"_id": str(user_id)},{"email": email}]})
        if user:
            return UserModel(**user)
        return None
