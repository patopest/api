from typing import List, Union

from xid import XID

from api.data.mapper import Mapper
from ..models import AuthModel


class AuthMapper(Mapper):
    def __init__(self, db):
        super().__init__(db)

    def insert(self, user: AuthModel) -> AuthModel:
        self._db.login.insert(user.to_dict())
        return self.find_by_id(user.id)

    def find(self, user_id: Union[XID, str]) -> Union[AuthModel, None]:
        user = self._db.login.find_one({"user_id": str(user_id)})
        if user:
            return AuthModel(**user)
        return None

    def find_by_id(self, _id: Union[XID, str]) -> Union[AuthModel, None]:
        user = self._db.login.find_one({"_id": str(_id)})
        if user:
            return AuthModel(**user)
        return None

    def find_by_email(self, email: str) -> Union[AuthModel, None]:
        user = self._db.login.find_one({"email": email})
        if user:
            return AuthModel(**user)
        return None

    def update(self, user: AuthModel) -> AuthModel:
        self._db.login.update({"_id": str(user.id)}, user.to_dict(), upsert=True)
        return user

    def delete(self, user: AuthModel):
        self._db.login.find_one_and_delete({"_id": str(user.id)})
