from api.data.model import Model


class UserModel(Model):
    email: str = ""
    user_type: str = "user"

    def dump(self) -> dict:
    	model = super().to_dict()
    	output = {
    		"id": model['_id'],
    		"email": model['email'],
    		"type": model['user_type'],
    		"metadata": {
    			"created_at": model['created_at'],
    			"updated_at": model['updated_at'],
    			"deleted_at": model['deleted_at']
    		}
    	}
    	return output
