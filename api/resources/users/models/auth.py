from typing import Union
from xid import XID

from api.data.model import Model


class AuthModel(Model):
	user_id: str = None
	email: str
	password: str
