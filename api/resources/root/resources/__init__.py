import falcon

from .welcome import Welcome
from .things import Things


def get_routes(app: falcon.API):
	app.add_route('/', Welcome())
	app.add_route('/things', Things())
