from __future__ import annotations
from datetime import datetime

from pydantic import BaseModel, Field, validator
from xid import XID
from typing import Union

from api.media import json

BaseModel = BaseModel


class Model(BaseModel):
    id: Union[XID, str] = Field(default_factory=XID, alias="_id")
    created_at: datetime = Field(default_factory=datetime.now)
    deleted_at: datetime = None
    updated_at: datetime = None

    class Config:
        arbitrary_types_allowed = True
    
    @validator('id')
    def convert_id(cls, model_id):
        if isinstance(model_id, XID):
            return model_id
        return XID(model_id)

    def delete(self):
        self.deleted_at = datetime.now()

    def update(self, d: dict) -> Model:
        d["updated_at"] = datetime.now()
        o = self.to_dict()
        o.update(d)
        return self.parse_obj(o)

    def to_dict(self) -> dict:
        model = self.copy()
        model.id = model.id.string()
        return model.dict(by_alias=True)

    def to_json(self) -> bytes:
        return json.dumps(self.to_dict())
