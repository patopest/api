from __future__ import annotations

import pymongo

from api.config import settings

_MOCK_CLIENT = None


def setup() -> pymongo.MongoClient:
    global _MOCK_CLIENT
    kwargs = {
        "connect": False,
        "connectTimeoutMs": settings.get("MONGO_CONNECT_TIMEOUT_MS"),
        "readPreference": settings.get("MONGO_READ_PREFERENCE"),
        "socketTimeoutMS": settings.get("MONGO_SOCKET_TIMEOUT_MS"),
        "serverSelectionTimeoutMS": settings.get("MONGO_SERVER_SELECTION_TIMEOUT_MS"),
        "replicaSet": settings.get("MONGO_REPLICA_SET"),
    }

    if settings.get("ENV") == "PYTEST":
        try:
            import mongomock
        except ImportError:
            raise EnvironmentError("mongomock dependancy required for tests")

        if _MOCK_CLIENT is None:
            _MOCK_CLIENT = mongomock.MongoClient()
        return _MOCK_CLIENT
    return pymongo.MongoClient(settings.get("MONGO_URI"), **kwargs)



