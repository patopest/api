import argparse

from api.util.vyper import settings

settings = settings

parser = argparse.ArgumentParser(description="Runs the API")


#General
g = parser.add_argument_group("general")
g.add_argument("--app-name",
	type=str, 
	default="api", 
	help="Application and process name. (default %(default)s)",
)
g.add_argument("--env-name",
	type=str,
	default='LOCAL',
	choices=["LOCAL","DEV","PROD"],
	help="Environment to run as. (default %(default)s)",
)
g.add_argument("--environment-variables-prefix",
	type=str,
	default="app",
	help="Prefix for environment variables (default %(default)s)",
)
g.add_argument("--base-user",
	type=str,
	default="admin@bambinito.co",
	help="Default user (default %(default)s)",
)
g.add_argument("--base-password",
	type=str,
	default="aqszaqsz",
	help="Default password for base user (default %(default)s)",
)
g.add_argument("--notifier-password",
	type=str,
	default=None,
	help="The password for the email account used to send notifications (default %(default)s)",
)
g.add_argument("--secret-key",
	type=str,
	default="random-key",
	help="Secret key used by jwt for token generation (default %(default)s)",
)
g.add_argument("--token-validity",
	type=int,
	default=24,
	help="The validity of the generated tokens in hours (default %(default)s)",
)
g.add_argument("--task-rate",
	type=int,
	default=1,
	help="The value at which to run the tasks in minutes (default %(default)s)",
)
g.add_argument("--success-task-rate",
	type=int,
	default=5,
	help="The value at which to run the tasks after a successful run in minutes(default %(default)s)",
)

#Rapidjson
rj = parser.add_argument_group('Rapidjson')
rj.add_argument(
    "--json-number-mode",
    type=str,
    default=None,
    help="Enable particular behaviors in handling numbers (default %(default)s)",
)
rj.add_argument(
    "--json-datetime-mode",
    type=str,
    default="DM_ISO8601",
    help="How should datetime, time and date instances be handled (default %(default)s)",
)
rj.add_argument(
    "--json-uuid-mode",
    type=str,
    default="UM_HEX",
    help="How should UUID instances be handled (default %(default)s)",
)

#Gunicorn
gu = parser.add_argument_group("Gunicorn")
gu.add_argument("--access-log",
	type=str,
	default="-",
	help="Where to store the access logs. (default %(default)s)",
	)
gu.add_argument("--access-log-format",
	type=str,
	default='%(h)s %(l)s %(u)s "%(r)s" %(s)s %(b)s "%(f)s" ',
	help="Gunicorn access log format. (default %(default)s)",
	)
gu.add_argument("--bind",
	type=str,
	default="127.0.0.1:5000",
	help="The socket to bind to. (default %(default)s)",
	)
gu.add_argument("--error-log",
	type=str,
	default="-",
	help="Where to store the error logs. (default %(default)s)",
	)
gu.add_argument("--keep-alive",
	type=int,
	default=650,
	help="The number of seconds to wait for requests on a Keep-Alive connection. (default %(default)s)",
	)
gu.add_argument("--max-requests",
	type=int,
	default=0,
	help="The maximum number of requests a worker will process before restarting. (default %(default)s)",
	)
gu.add_argument("--max-requests-jitter",
	type=int,
	default=0,
	help="The maximum jitter to add to the max_requests setting. (default %(default)s)",
	)
gu.add_argument("--timeout",
	type=int,
	default=240,
	help="Workers silent for more than this many seconds are killed and restarted. (default %(default)s)",
	)
gu.add_argument("--worker-class",
	type=str,
	default="egg:meinheld#gunicorn_worker",
	help="The type of worker to use. (default %(default)s)",
	)
gu.add_argument("--workers",
	type=int,
	default=1,
	help="The number of workers to use. (default %(default)s)",
	)
gu.add_argument("--reload",
	action='store_true',
	default=False,
	help="Reload Gunicorn if code is changed (default %(default)s)",
	)


#Logs
l = parser.add_argument_group("Logs")
l.add_argument("--log-format",
	type=str,
	default="[%(asctime)s] [%(process)d] [%(levelname)s] %(name)s - %(message)s",
	help="Log format (default %(default)s)",
	)
l.add_argument("--log-date-format",
	type=str,
	default="%Y-%m-%d %H:%M:%S %z",
	help="Log date format (default %(default)s)",
	)
l.add_argument("--log-handlers",
	type=str,
	default="console",
	help="Log handlers (default %(default)s)",
	)
l.add_argument("--log-level",
	type=str,
	default="DEBUG",
	help="Log level (default %(default)s)",
)


#MongoDB
m = parser.add_argument_group("MongoDB")
m.add_argument("--mongo-db-name",
	type=str,
	default="minerva",
	help="MongoDB database name (default %(default)s)",
	)
m.add_argument("--mongo-uri",
	type=str,
	default="mongodb://localhost:27017/",
	help="MongoDB URI (default %(default)s)",
	)
m.add_argument("--mongo-username",
	type=str,
	default=None,
	help="Mongo username (default %(default)s)",
	)
m.add_argument("--mongo-password",
	type=str,
	default=None,
	help="Mongo passwords (default %(default)s)",
	)
m.add_argument("--mongo-replica-set",
	type=str,
	default=None,
	help="MongoDB Replica set (default %(default)s)",
	)
m.add_argument("--mongo-connect-timeout-ms",
	type=int,
	default=5000,
	help="MongoDB connect timeout (default %(default)s)",
	)
m.add_argument("--mongo-read-preference",
	type=str,
	default="primary",
	help="MongoDB replica set read preference (primary, primaryPreferred, secondary, secondaryPreferred, nearest). (default %(default)s)",
	)
m.add_argument("--mongo-server-selection-timeout-ms",
	type=int,
	default=5000,
	help="MongoDB server selection timeout (default %(default)s)",
	)
m.add_argument("--mongo-socket-timeout-ms",
	type=int,
	default=30000,
	help="MongoDB socket timout (default %(default)s)",
)








