import smtplib
from email.message import EmailMessage
import logging

from api.config import settings

logger = logging.getLogger(__name__)



def notify(course, crn, spaces, recipients):
	if settings.get("NOTIFIER_PASSWORD") is None:
		logger.error("No password provided for notifier")
		return
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login("bambinito.dev@gmail.com", settings.get("NOTIFIER_PASSWORD"))

	body = "The course " + course + " (" + crn + "), has " + str(spaces) + " seats remaining."

	msg = EmailMessage()
	msg["Subject"] = "{} has a seat available".format(course)
	msg["From"] = "Minerva App <bambinito.dev@gmail.com>"
	msg["To"] = recipients
	msg.set_content(body)
	
	server.send_message(msg)
	logger.info("Email sent to {} for course {}".format(recipients, course))
	server.quit()


def test_notification(recipient):
	if settings.get("NOTIFIER_PASSWORD") is None:
		logger.error("No password provided for notifier")
		return
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login("bambinito.dev@gmail.com", settings.get("NOTIFIER_PASSWORD"))

	body = "This is a test notification to ensure you do receive emails from the Minerva App."

	msg = EmailMessage()
	msg["Subject"] = "Test notification from Minerva App"
	msg["From"] = "Minerva App <bambinito.dev@gmail.com>"
	msg["To"] = recipient
	msg.set_content(body)
	
	server.send_message(msg)
	logger.info("Test notification sent to {}".format(recipient))
	server.quit()