import logging
from datetime import datetime

#from app import get_scheduler as scheduler

from ..minerva.minerva import minerva_task
from .notifier import notify

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.mongodb import MongoDBJobStore

from api.config import settings
from ..data.db import setup as setup_mongo

logger = logging.getLogger(__name__)

class Scheduler:
	def __init__(self):
		jobstore = MongoDBJobStore(database='minerva', collection='apscheduler_jobs', client=setup_mongo())

		self.scheduler = BackgroundScheduler()
		self.scheduler.add_jobstore(jobstore)
		self.scheduler.start()

		self.scheduler_rate = settings.get('TASK_RATE')

	def create_task(self, task_id):
		try:
			#minerva_task(course['course'])
			job = self.scheduler.add_job(task, "interval", args=[task_id], minutes=self.scheduler_rate, id=task_id)
			return True
		except:
			logger.error("Error trying to create scheduler job for {}".format(task_id))
			return False

	def get_task(self, task_id):
		job = self.scheduler.get_job(job_id=task_id)
		if job:
			return True
		else:
			return False

	def edit_task(self, task_id):
		try:
			job = self.scheduler.modify_job(job_id=task_id, args=[task_id])
			return True
		except:
			logger.error("Error trying to edit scheduler job for {}".format(task_id))
			return False

	def reschedule_task(self, task_id, minutes=None):
		task_rate = minutes or self.scheduler_rate
		try:
			job = self.scheduler.reschedule_job(job_id=task_id, trigger="interval", minutes=task_rate)
			return True
		except:
			logger.error("Error trying to reschedule job for {}".format(task_id))
			return False

	def delete_task(self, task_id):
		job = self.scheduler.get_job(job_id=task_id)
		if job:
			self.scheduler.remove_job(job_id=task_id)

	def delete_tasks(self):
		deleted = 0
		for job in self.scheduler.get_jobs():
			job.remove()
			deleted += 1
		return deleted

scheduler = None

def setup_scheduler():
	global scheduler
	scheduler = Scheduler()

def get_scheduler():
	return scheduler


def task(task_id):

	from ..resources.tasks.mappers import TaskMapper
	from api.data import retrieve_model
	from api.data.db import setup

	db = setup()['minerva']
	mapper = TaskMapper(db)
	task = retrieve_model(task_id, mapper)

	body = task.dump()
	course = body['course']

	updater = {
		"last_run": datetime.now(),
	} 

	spaces = minerva_task(course, body['user'])

	#logger.info("Found {} spaces for course: {} for user: {}".format(spaces, course['dept']+course['code'], body['user']))
	if spaces > 0:
		course_text = "{}-{}".format(course['dept'],course['code'])
		notify(course_text, course['crn'], spaces, body['email'])
	
	# Handling rescheduling to avoid email spamming
	if (spaces > 0) and (body['rate'] == settings.get('TASK_RATE')):
		updater['rate'] = settings.get('SUCCESS_TASK_RATE')
		get_scheduler().reschedule_task(task_id, minutes=updater['rate'])
	else:
		updater['rate'] = settings.get('TASK_RATE')
		get_scheduler().reschedule_task(task_id, minutes=updater['rate'])

	task = task.update(updater)
	mapper.update(task)


